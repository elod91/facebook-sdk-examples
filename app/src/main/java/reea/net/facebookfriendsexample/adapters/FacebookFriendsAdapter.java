package reea.net.facebookfriendsexample.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import reea.net.facebookfriendsexample.R;
import reea.net.facebookfriendsexample.social.models.friends.FacebookFriend;

public class FacebookFriendsAdapter extends RecyclerView.Adapter<FacebookFriendsAdapter.ViewHolder> {
    private List<FacebookFriend> mData;
    private Activity mActivity;

    public FacebookFriendsAdapter(Activity activity) {
        mData = new ArrayList<>();
        mActivity = activity;
    }

    public void addData(List<FacebookFriend> facebookFriends) {
        mData.addAll(facebookFriends);
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_facebook_friend, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        FacebookFriend facebookFriend = mData.get(position);

        holder.friendName.setText(facebookFriend.getName());
        Glide.with(mActivity).load(facebookFriend.getPicture().getPictureData().getPictureUrl()).into(holder.friendImage);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView friendImage;
        public TextView friendName;

        public ViewHolder(View itemView) {
            super(itemView);

            friendImage = (ImageView) itemView.findViewById(R.id.facebook_friend_image);
            friendName = (TextView) itemView.findViewById(R.id.facebook_friend_name);
        }
    }
}
