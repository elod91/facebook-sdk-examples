package reea.net.facebookfriendsexample.social.interfaces;

public interface FacebookLoginCallback {
    void onSuccess(String accessToken);

    void onError(int errorCode);

    void onCancel();
}
