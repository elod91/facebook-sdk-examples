package reea.net.facebookfriendsexample.social.helper;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import reea.net.facebookfriendsexample.social.FacebookResponseErrorCodes;
import reea.net.facebookfriendsexample.social.interfaces.FacebookFriendsCallback;
import reea.net.facebookfriendsexample.social.interfaces.FacebookLoginCallback;
import reea.net.facebookfriendsexample.social.interfaces.FacebookPictureCallback;
import reea.net.facebookfriendsexample.social.interfaces.FacebookUserProfileCallback;
import reea.net.facebookfriendsexample.social.models.FacebookFriendsResponse;
import reea.net.facebookfriendsexample.social.models.FacebookPictureResponse;
import reea.net.facebookfriendsexample.utils.JsonUtil;

public class FacebookHelper {
    public static final int FACEBOOK_FRIENDS_LIMIT = 25;

    private static FacebookHelper INSTANCE;

    private CallbackManager mCallbackManager;

    private Activity mActivity;

    private FacebookLoginCallback mLoginCallback;
    private FacebookFriendsCallback mFriendsCallback;
    private FacebookPictureCallback mPictureCallback;
    private FacebookUserProfileCallback mUserProfileCallback;

    /**
     * @param activity Activity which has onActivityResult implemented and calls the UFacebook's onActivityResult method. THIS IS VERY IMPORTANT!!!
     */
    public static synchronized FacebookHelper getInstance(Activity activity) {
        if (INSTANCE == null || INSTANCE.getActivity() != activity)
            INSTANCE = new FacebookHelper(activity);

        return INSTANCE;
    }

    private FacebookHelper(Activity activity) {
        mActivity = activity;

        FacebookSdk.sdkInitialize(mActivity);

        mCallbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                if (mLoginCallback != null)
                    mLoginCallback.onSuccess(loginResult.getAccessToken().getToken());
            }

            @Override
            public void onCancel() {
                // User cancel the facebook login
                if (mLoginCallback != null)
                    mLoginCallback.onCancel();
            }

            @Override
            public void onError(FacebookException e) {
                // Error occurred during the facebook login
                if (mLoginCallback != null)
                    mLoginCallback.onError(FacebookResponseErrorCodes.LOGIN_RESPONSE_ERROR);
            }
        });
    }

    /**
     * Sets the facebook login callback.
     */
    public void setFacebookLoginCallback(FacebookLoginCallback facebookLoginCallback) {
        mLoginCallback = facebookLoginCallback;
    }

    /**
     * Sets the facebook friends callback.
     */
    public void setFacebookFriendsCallback(FacebookFriendsCallback facebookFriendsCallback) {
        mFriendsCallback = facebookFriendsCallback;
    }

    /**
     * Sets the facebook picture callback.
     */
    public void setFacebookPictureCallback(FacebookPictureCallback facebookPictureCallback) {
        mPictureCallback = facebookPictureCallback;
    }

    /**
     * Sets the facebook user profile callback.
     */
    public void setFacebookUserProfileCallback(FacebookUserProfileCallback facebookUserProfileCallback) {
        mUserProfileCallback = facebookUserProfileCallback;
    }

    /**
     * IMPORTANT: This method needs to be implemented in the host Activity's onActivityResult method!!!
     */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Starts the facebook login.
     */
    public void signInWithFacebook() {
        if (AccessToken.getCurrentAccessToken() != null) {
            if (mLoginCallback != null)
                mLoginCallback.onSuccess(AccessToken.getCurrentAccessToken().getToken());
            return;
        }

        LoginManager.getInstance().logInWithReadPermissions(mActivity, Arrays.asList("public_profile", "email", "user_friends"));
    }

    /**
     * Signs out of facebook (clears the session).
     */
    public void singOutOfFacebook() {
        if (AccessToken.getCurrentAccessToken() == null)
            return;

        LoginManager.getInstance().logOut();
    }

    /**
     * After login was successful and we have an access token, the method makes a request to get information about the logged in user.
     */
    public void getMe() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if (accessToken == null || accessToken.isExpired()) {
            if (mUserProfileCallback != null)
                mUserProfileCallback.tokenExpired();
            return;
        }

        // Setup the request, add the callback.
        GraphRequest meRequest = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject jsonObject, GraphResponse graphResponse) {
                if (mLoginCallback == null)
                    return;

                try {
                    // Get back the values from the response object
                    mUserProfileCallback.onSuccess(jsonObject.getString("name"), jsonObject.getString("email"), jsonObject.getString("id"));
                } catch (JSONException e) {
                    mLoginCallback.onError(FacebookResponseErrorCodes.JSON_PARSE_ERROR);
                } catch (Exception e) {
                    mUserProfileCallback.tokenExpired();
                }
            }
        });

        // Setup the params. Insert what we want to get back from the request.
        Bundle params = new Bundle();
        params.putString("fields", "id, name, email");

        meRequest.setParameters(params);
        meRequest.executeAsync();
    }

    public void getFriends(int currentPage) {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if (accessToken == null || accessToken.isExpired()) {
            if (mFriendsCallback != null)
                mFriendsCallback.tokenExpired();
            return;
        }

        GraphRequest friendsRequest = new GraphRequest(
                accessToken,
                "/me/friends",
                null,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        if (response.getError() != null) {
                            if (mFriendsCallback != null)
                                mFriendsCallback.onError(FacebookResponseErrorCodes.FRIENDS_LIST_RESPONSE_ERROR);
                            return;
                        }

                        try {
                            FacebookFriendsResponse facebookFriendsResponse = JsonUtil.jsonToObject(response.getRawResponse(), FacebookFriendsResponse.class);
                            if (mFriendsCallback != null)
                                mFriendsCallback.onSuccess(facebookFriendsResponse);
                        } catch (JSONException e) {
                            if (mFriendsCallback != null)
                                mFriendsCallback.onError(FacebookResponseErrorCodes.JSON_PARSE_ERROR);
                        } catch (Exception e) {
                            mUserProfileCallback.tokenExpired();
                        }
                    }
                }
        );

        Bundle params = new Bundle();
        params.putString("fields", "picture,name");
        params.putInt("limit", FACEBOOK_FRIENDS_LIMIT);
        params.putInt("offset", FACEBOOK_FRIENDS_LIMIT * currentPage);

        friendsRequest.setParameters(params);
        friendsRequest.executeAsync();
    }

    public void getUserPicture(String userId) {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if (accessToken == null || accessToken.isExpired()) {
            if (mPictureCallback != null)
                mPictureCallback.tokenExpired();
            return;
        }

        GraphRequest pictureRequest = new GraphRequest(
                accessToken,
                "/" + userId + "/picture",
                null,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        if (response.getError() != null) {
                            if (mPictureCallback != null)
                                mPictureCallback.onError(FacebookResponseErrorCodes.USER_PICTURE_RESPONSE_ERROR);
                            return;
                        }

                        try {
                            FacebookPictureResponse facebookPictureResponse = JsonUtil.jsonToObject(response.getRawResponse(), FacebookPictureResponse.class);
                            if (mPictureCallback != null)
                                mPictureCallback.onSuccess(facebookPictureResponse);
                        } catch (JSONException e) {
                            if (mPictureCallback != null)
                                mPictureCallback.onError(FacebookResponseErrorCodes.JSON_PARSE_ERROR);
                        } catch (Exception e) {
                            mUserProfileCallback.tokenExpired();
                        }
                    }
                }
        );

        Bundle params = new Bundle();
        params.putString("type", "large");

        pictureRequest.setParameters(params);
        pictureRequest.executeAsync();
    }

    private Activity getActivity() {
        return mActivity;
    }
}
