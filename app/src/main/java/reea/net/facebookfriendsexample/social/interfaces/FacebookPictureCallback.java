package reea.net.facebookfriendsexample.social.interfaces;

import reea.net.facebookfriendsexample.social.models.FacebookPictureResponse;

public interface FacebookPictureCallback extends BaseFacebookCallback {

    void onSuccess(FacebookPictureResponse facebookPictureResponse);
}
