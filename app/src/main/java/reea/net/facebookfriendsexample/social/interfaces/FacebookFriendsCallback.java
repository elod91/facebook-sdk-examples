package reea.net.facebookfriendsexample.social.interfaces;

import reea.net.facebookfriendsexample.social.models.FacebookFriendsResponse;

public interface FacebookFriendsCallback extends BaseFacebookCallback {
    void onSuccess(FacebookFriendsResponse facebookFriendsResponse);
}
