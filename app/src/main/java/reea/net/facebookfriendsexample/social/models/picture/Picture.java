package reea.net.facebookfriendsexample.social.models.picture;

import com.google.gson.annotations.SerializedName;

public class Picture {
    @SerializedName("is_silhouette")
    private boolean silhouette;
    @SerializedName("url")
    private String pictureUrl;

    public boolean isSilhouette() {
        return silhouette;
    }

    public void setSilhouette(boolean silhouette) {
        this.silhouette = silhouette;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    @Override
    public String toString() {
        return "Picture{" +
                "silhouette=" + silhouette +
                ", pictureUrl='" + pictureUrl + '\'' +
                '}';
    }
}
