package reea.net.facebookfriendsexample.social.models.friends;

import reea.net.facebookfriendsexample.social.models.FacebookPictureResponse;

public class FacebookFriend {
    private String id;
    private String name;
    private FacebookPictureResponse picture;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public FacebookPictureResponse getPicture() {
        return picture;
    }

    public void setPicture(FacebookPictureResponse picture) {
        this.picture = picture;
    }

    @Override
    public String toString() {
        return "FacebookFriend{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", picture=" + picture +
                '}';
    }
}
