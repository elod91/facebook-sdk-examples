package reea.net.facebookfriendsexample.social.models.friends;

import com.google.gson.annotations.SerializedName;

public class Summary {
    @SerializedName("total_count")
    private int totalCount;

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    @Override
    public String toString() {
        return "Summary{" +
                "totalCount=" + totalCount +
                '}';
    }
}
