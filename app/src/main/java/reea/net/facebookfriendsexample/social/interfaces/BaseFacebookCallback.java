package reea.net.facebookfriendsexample.social.interfaces;

public interface BaseFacebookCallback {
    void tokenExpired();

    void onError(int errorCode);
}
