package reea.net.facebookfriendsexample.social.interfaces;

public interface FacebookUserProfileCallback extends BaseFacebookCallback {
    void onSuccess(String name, String email, String id);
}
