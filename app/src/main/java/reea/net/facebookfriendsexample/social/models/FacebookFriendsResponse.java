package reea.net.facebookfriendsexample.social.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import reea.net.facebookfriendsexample.social.models.friends.FacebookFriend;
import reea.net.facebookfriendsexample.social.models.friends.Paging;
import reea.net.facebookfriendsexample.social.models.friends.Summary;

public class FacebookFriendsResponse {
    @SerializedName("data")
    private List<FacebookFriend> friendList;
    private Summary summary;
    private Paging paging;

    public List<FacebookFriend> getFriendList() {
        return friendList;
    }

    public void setFriendList(List<FacebookFriend> friendList) {
        this.friendList = friendList;
    }

    public Summary getSummary() {
        return summary;
    }

    public void setSummary(Summary summary) {
        this.summary = summary;
    }

    public Paging getPaging() {
        return paging;
    }

    public void setPaging(Paging paging) {
        this.paging = paging;
    }

    @Override
    public String toString() {
        return "FacebookFriendsResponse{" +
                "friendList=" + friendList +
                ", summary=" + summary +
                ", paging=" + paging +
                '}';
    }
}
