package reea.net.facebookfriendsexample.social.models;

import com.google.gson.annotations.SerializedName;

import reea.net.facebookfriendsexample.social.models.picture.Picture;

public class FacebookPictureResponse {
    @SerializedName("data")
    private Picture pictureData;

    public Picture getPictureData() {
        return pictureData;
    }

    public void setPictureData(Picture pictureData) {
        this.pictureData = pictureData;
    }
}
