package reea.net.facebookfriendsexample.social;

public interface FacebookResponseErrorCodes {
    int LOGIN_RESPONSE_ERROR = 0x1;
    int FRIENDS_LIST_RESPONSE_ERROR = 0x2;
    int USER_PICTURE_RESPONSE_ERROR = 0x3;

    int JSON_PARSE_ERROR = 0x4;
}
