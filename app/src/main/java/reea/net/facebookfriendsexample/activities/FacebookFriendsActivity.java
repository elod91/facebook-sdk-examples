package reea.net.facebookfriendsexample.activities;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import reea.net.facebookfriendsexample.R;
import reea.net.facebookfriendsexample.activities.base.BaseActivity;
import reea.net.facebookfriendsexample.adapters.FacebookFriendsAdapter;
import reea.net.facebookfriendsexample.social.FacebookResponseErrorCodes;
import reea.net.facebookfriendsexample.social.helper.FacebookHelper;
import reea.net.facebookfriendsexample.social.interfaces.FacebookFriendsCallback;
import reea.net.facebookfriendsexample.social.interfaces.FacebookLoginCallback;
import reea.net.facebookfriendsexample.social.models.FacebookFriendsResponse;
import reea.net.facebookfriendsexample.utils.DividerItemDecoration;
import reea.net.facebookfriendsexample.utils.EndlessRecyclerOnScrollListener;

public class FacebookFriendsActivity extends BaseActivity implements FacebookFriendsCallback, FacebookLoginCallback {
    private RecyclerView facebookFriendsList;
    private FacebookFriendsAdapter facebookFriendsAdapter;
    private LinearLayoutManager layoutManager;

    private ProgressBar friendLoadingProgress;
    private TextView friendsEmpty;

    private EndlessRecyclerOnScrollListener endlessRecyclerOnScrollListener;

    private int currentPage = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facebook_friends);

        friendLoadingProgress = (ProgressBar) findViewById(R.id.friend_loading_progress);
        friendsEmpty = (TextView) findViewById(R.id.facebook_friends_empty);

        setRecyclerView();
        FacebookHelper.getInstance(this).setFacebookFriendsCallback(this);
        FacebookHelper.getInstance(this).getFriends(currentPage);
    }

    @Override
    protected String getToolbarTitle() {
        return "Facebook friends";
    }

    @Override
    public void tokenExpired() {
        FacebookHelper.getInstance(this).signInWithFacebook();
    }

    @Override
    public void onSuccess(FacebookFriendsResponse facebookFriendsResponse) {
        if (facebookFriendsResponse.getPaging() == null || facebookFriendsResponse.getPaging().getNext() == null || facebookFriendsResponse.getPaging().getNext().isEmpty())
            facebookFriendsList.clearOnScrollListeners();

        friendLoadingProgress.setVisibility(View.GONE);
        if (facebookFriendsResponse.getFriendList() == null || facebookFriendsResponse.getFriendList().isEmpty()) {
            if (currentPage == 0) {
                facebookFriendsList.setVisibility(View.GONE);
                friendsEmpty.setVisibility(View.VISIBLE);
            }
            return;
        }

        facebookFriendsAdapter.addData(facebookFriendsResponse.getFriendList());
    }

    @Override
    public void onSuccess(String accessToken) {
        FacebookHelper.getInstance(this).getFriends(currentPage);
    }

    @Override
    public void onError(int errorCode) {
        switch (errorCode) {
            case FacebookResponseErrorCodes.LOGIN_RESPONSE_ERROR:
                Toast.makeText(this, "Error occurred. Please try again.", Toast.LENGTH_SHORT).show();
                break;

            case FacebookResponseErrorCodes.FRIENDS_LIST_RESPONSE_ERROR:
                Toast.makeText(this, "Could not get friend list. Please try again later.", Toast.LENGTH_SHORT).show();
                break;

            case FacebookResponseErrorCodes.JSON_PARSE_ERROR:
                Toast.makeText(this, "Got back an invalid response. Please try again.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onCancel() {
        Toast.makeText(this, "Login denied...", Toast.LENGTH_SHORT).show();
    }

    private void setRecyclerView() {
        facebookFriendsList = (RecyclerView) findViewById(R.id.facebook_friend_list);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        facebookFriendsList.setHasFixedSize(true);

        // use a linear layout manager
        layoutManager = new LinearLayoutManager(this);

        endlessRecyclerOnScrollListener = new EndlessRecyclerOnScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                currentPage = current_page;

                friendLoadingProgress.setVisibility(View.VISIBLE);
                FacebookHelper.getInstance(FacebookFriendsActivity.this).getFriends(currentPage);
            }
        };

        facebookFriendsAdapter = new FacebookFriendsAdapter(this);

        facebookFriendsList.setLayoutManager(layoutManager);
        facebookFriendsList.setAdapter(facebookFriendsAdapter);
        facebookFriendsList.addOnScrollListener(endlessRecyclerOnScrollListener);
        facebookFriendsList.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL_LIST));
    }
}
