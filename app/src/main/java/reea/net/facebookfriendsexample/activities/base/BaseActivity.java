package reea.net.facebookfriendsexample.activities.base;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import reea.net.facebookfriendsexample.R;

public abstract class BaseActivity extends AppCompatActivity {
    protected Toolbar toolbar;
    protected TextView toolbarTitle;

    @Override
    protected void onResume() {
        super.onResume();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setToolbarTitle();
    }

    private void setToolbarTitle() {
        toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText(getToolbarTitle());
    }

    protected abstract String getToolbarTitle();
}
