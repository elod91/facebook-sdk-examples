package reea.net.facebookfriendsexample.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import reea.net.facebookfriendsexample.R;
import reea.net.facebookfriendsexample.activities.base.BaseActivity;
import reea.net.facebookfriendsexample.social.FacebookResponseErrorCodes;
import reea.net.facebookfriendsexample.social.helper.FacebookHelper;
import reea.net.facebookfriendsexample.social.interfaces.FacebookLoginCallback;
import reea.net.facebookfriendsexample.utils.KeyGeneratorUtil;

public class LoginActivity extends BaseActivity implements FacebookLoginCallback, View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setOnClickListeners();

        KeyGeneratorUtil.generateFacebookDevelopmentHash(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        FacebookHelper.getInstance(this).setFacebookLoginCallback(this);
    }

    @Override
    protected String getToolbarTitle() {
        return "Login";
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        FacebookHelper.getInstance(this).onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onSuccess(String accessToken) {
        startActivity(new Intent(this, FacebookFriendsActivity.class));
        finish();
    }

    @Override
    public void onError(int errorCode) {
        switch (errorCode) {
            case FacebookResponseErrorCodes.LOGIN_RESPONSE_ERROR:
                Toast.makeText(this, "Error occurred. Please try again.", Toast.LENGTH_SHORT).show();
                break;

            case FacebookResponseErrorCodes.JSON_PARSE_ERROR:
                Toast.makeText(this, "Got back an invalid response. Please try again.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onCancel() {
        Toast.makeText(this, "Login denied...", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_facebook:
                FacebookHelper.getInstance(this).signInWithFacebook();
        }
    }

    private void setOnClickListeners() {
        findViewById(R.id.login_facebook).setOnClickListener(this);
    }
}
